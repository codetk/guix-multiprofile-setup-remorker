source "$HOME/.config/guix-multiprof/etc/param.sh"

declare -A prfs
for nm in ${PROFILES[@]};do
  prfs[$nm]="$PROFDIR"/$nm
done

# The default profile.
GUIX_PROFILE=$HOME/.guix-profile
. "$GUIX_PROFILE"/etc/profile
XDG_DATA_DIRS="$GUIX_PROFILE/share:$XDG_DATA_DIRS"
XDG_CONFIG_DIRS="$GUIX_PROFILE/etc/xdg:$XDG_CONFIG_DIRS"


# The profiles.
for dr in ${prfs[@]}; do
   export GUIX_PROFILE=$dr
   . "$GUIX_PROFILE/etc/profile"
   XDG_DATA_DIRS="$GUIX_PROFILE/share:$XDG_DATA_DIRS"
   XDG_CONFIG_DIRS="$GUIX_PROFILE/etc/xdg:$XDG_CONFIG_DIRS"
done
export GI_TYPELIB_PATH=${GI_TYPELIB_PATH:+} #Unsetting this because it breaks GNOME

export XDG_DATA_DIRS
export XDG_CONFIG_DIRS
export GUIX_PROFILE="$HOME/.guix-profile"
export GUIX_LOCPATH="${prfs[pillar]}/lib/locale"
export INFOPATH="$INFOPATH:$HOME/.config/guix/current/share/info:/usr/local/share/info"
export GUIX_LD_WRAPPER_ALLOW_IMPURITIES=no
export SSL_CERT_DIR="$PILLARDIR/etc/ssl/certs"
export SSL_CERT_FILE="$PILLARDIR/etc/ssl/certs/ca-certificates.crt"
export SSL_CERT_FILE="$PILLARDIR/etc/ssl/certs/ca-certificates.crt"
export PATH="$HOME/.config/guix/current/bin${PATH:+:}$PATH"
R_LIBS_USER=
export R_LIBS_USER

# # There was an ess glitch that used to be taken care of with the
# # line below. However, it seems that they fixed it since then.
# if [ -d "$HOME/.guix-multiprof/emacs/share/emacs/site-lisp/guix.d/ess" ]; then
#     export EMACSLOADPATH="$HOME/.guix-multiprof/emacs/share/emacs/site-lisp/guix.d/ess:$EMACSLOADPATH"
# fi

# Due to a conflict between "pillar" and standard profile GIT_EXEC_PATH is set wrongly
# This is an ugly fix:
export GIT_EXEC_PATH=$PILLARDIR/libexec/git-core

if [ -e "$GUIXFONTFILE" ]; then 
  xset +fp $(dirname $(readlink -f "$GUIXFONTFILE"))
fi
hash guix
ulimit -s unlimited
