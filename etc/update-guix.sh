#!/bin/bash

source "$HOME/.config/guix-multiprof/etc/param.sh"

mkdir -p "$PROFDIR"
mkdir -p $HOME/.config/guix
cp "$MANIFDIR/etc/channels.scm" "$HOME/.config/guix"

${THEGUIX} pull --disable-authentication -c${NCPU} 

for prof in ${PROFILES[@]};do
    echo ">>>>>>>>> START: $prof <<<<<<<<<"
    ${THEGUIX} package --fallback -c${NCPU} -p "$PROFDIR/$prof" -m "$MANIFDIR/${prof}.scm"
    echo ">>>>>>>>> FINISH: $prof <<<<<<<<<"
done

if [ -d "${PILLARDIR}/fonts" ]; then
    fc-cache -rv "${PILLARDIR}/fonts"
fi
