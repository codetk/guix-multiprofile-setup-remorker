source "$HOME/.config/guix-multiprof/etc/param.sh"
source "$MANIFDIR/etc/enable.sh"
mkdir -p "$HOME/.emacs.d"
touch "$HOME/.emacs.d/custom.el"
cp "$MANIFDIR/etc/.emacs" "$HOME"
cp "$MANIFDIR/etc/.emacs.org" "$HOME"

emacs --batch --eval "(require 'org)" --eval "(org-babel-tangle-file \"$HOME/.emacs.org\")"
