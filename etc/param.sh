MANIFDIR=$HOME/.config/guix-multiprof
PROFDIR=$HOME/.guix-pillars
PROFILES=(pillar browsers)
THEGUIX=$HOME/.config/guix/current/bin/guix
ROOTGUIX=/usr/local/bin/guix
PILLARDIR=$PROFDIR/pillar
GUIXFONTFILE=$PILLARDIR/share/fonts/truetype/fonts.dir
NCPU=4
