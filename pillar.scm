(use-modules (guix packages))
(specifications->manifest
 `(;; Utilities
   "recutils"
   "curl"
   "git"
   "subversion"
   "bash"
   "guile"
   "nano"
   "nss-certs"
   "glibc-locales"
   "gcc-toolchain"
   "gfortran-toolchain"
   "python"
   "perl"
   "htop"
   "ansible"

   ;; Emacs
   "the-silver-searcher"
   "emacs"
   "emacs-bbdb"
   "emacs-use-package"
   "emacs-ag"
   "emacs-magit"
   "emacs-org"
   "emacs-org-ref"
   "emacs-paredit"
   "emacs-ess"
   "emacs-company"
   "emacs-ivy"
   "emacs-counsel"
   "emacs-geiser"
   "emacs-guix"
   "emacs-zenburn-theme"
   "emacs-solarized-theme"
   "emacs-cyberpunk-theme"
   "emacs-atom-one-dark-theme"
   "emacs-polymode"
   "emacs-polymode-org"
   "emacs-polymode-ansible"
   "emacs-csv"
   "emacs-smartparens"
   "emacs-elpy"
   "emacs-wgrep"
   "emacs-emojify"

   ;; R
   "r"
   "r-curl"
   "r-ellipsis"
   "r-remoter"
   "r-future"
   "r-rmarkdown"
   "r-knitr"
   "r-roxygen2"
   "r-rcolorbrewer"
   "r-data-table"
   "r-gplots"
   "r-devtools"
   "r-usethis"
   "r-shiny"
   "r-shinyfiles"
   "r-shinydashboard"
   "r-flexdashboard"
   "r-handsontable"
   "r-shinyfiles"
   "r-cowplot"
   "r-withr"
   "r-massbank"
   "r-chemmass"
   "r-resolution"
   "r-msnbase"
   "r-pander"

   ;; Rust
   "rust"
   "rust:cargo"

   ;; X
   "xterm"
   "rxvt-unicode"
   "evince"
   "xdg-utils"
   "xdg-user-dirs"
   "pango"
   "libxft"
   "cairo"

   ;; TeX
   "gs-fonts"
   "texlive"
   ;; "gs-fonts"
   ;; "texlive-charter"
   ;; "texlive-fonts-stmaryrd"
   ;; "texlive-txfonts"
   ;; "texlive-amsfonts"
   ;; "texlive-fonts-latex"
   ;; "texlive-latex-psnfss"
   ;; "texlive-latex-type1cm"
   ;; "texlive-latex-ms"
   ;; "texlive-fontinst"
   ;; "texlive-metafont-base"
   ;; "texlive-latex-fontspec"
   ;; "texlive-hyph-utf8"
   ;; "lyx"
   ;; "texlive-bin"
   ;; "texlive-base"
   ;; "texlive-latex-tools"
   ;; "texlive-latex-base"
   ;; "texlive-latexconfig"
   ;; "texlive-dvips"
   ;; "texlive-fonts-latex"
   ;; "texlive-latex-babel"
   ;; "texlive-latex-graphics"
   ;; "texlive-amsfonts"
   ;; "texlive-fonts-ec"
   ;; "texlive-dvips"
   ;; "texlive-bibtex"
   ;; "texlive-latex-fancyhdr"
   ;; "texlive-latex-enumitem"
   ;; "texlive-latex-ifplatform"
   ;; "texlive-latex-koma-script"
   ;; "texlive-url"
   ;; "texlive-latex-framed"
   ;; "texlive-latex-geometry"
   ;; "texlive-latex-hyperref"

   ;;Fonts
   "unicode-emoji"
   "fontconfig"
   "font-dejavu"
   "font-gnu-freefont-ttf"
   "font-ubuntu"
   "font-terminus"
   "font-liberation"
   "font-inconsolata"
   "font-gnu-unifont"
   "font-public-sans"
   "font-misc-misc"
   "font-awesome"
   "font-hack"
   "font-iosevka"
   "font-iosevka-term"
   "font-iosevka-term-slab"
   "font-iosevka-sparkle"
   "font-iosevka-slab"
   "font-iosevka-etoile"
   "font-iosevka-aile"
   "font-google-noto"

   ;;Python
   "python-requests"
   "python-ruamel.yaml"
   "python-matplotlib"
   "python-numpy"
   "python-scipy"

   ;; Java
   "maven"
   "openjdk"

   ;; Perl
   "perl-yaml-libyaml"))
 
